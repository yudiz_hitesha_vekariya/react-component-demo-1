import React from "react";
import ReactDOM from "react-dom";
import Sdata from "./Sdata";
import Card1 from "./card";
import "./index.css";

function ncard(val, index, arr) {
  return (
    <Card1
      key={val.id}
      imgsrc={val.imgsrc}
      titles={val.titles}
      sname={val.sname}
      link={val.link}
    />
  );
}
ReactDOM.render(<>{Sdata.map(ncard)}</>, document.getElementById("root"));
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
