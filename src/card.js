import React from "react";

function Card(props) {
  return (
    <div className="cards">
      <div className="card">
        <img src={props.imgsrc} alt="" />
        <div className="card_info">
          <span className="food_name">{props.titles} </span>
          <h3 className="card_title">{props.sname} </h3>
          <a href={props.link} target="blank">
            <button className="btn" type="submit">
              Show now
            </button> 
          </a>
        </div>
      </div>
    </div>
  );
}

export default Card;
